package sbu.cs;

import java.util.Locale;

public class ExerciseLecture4 {

    /*
     *   implement a function that returns factorial of given n
     *   lecture 4 page 15
     */
    public long factorial(int n) {
        long answer = 1;
        for (int i =1 ; i <= n ; i++)
        {
            answer *= i;
        }
        return answer;
    }

    /*
     *   implement a function that return nth number of fibonacci series
     *   the series -> 1, 1, 2, 3, 5, 8, ...
     *   lecture 4 page 19
     */
    public long fibonacci(int n) {
        long a = 1 , b = 1;
        if (n == 1 || n == 2){
            return 1;
        }
        else
        {
            long c = a;
            for (int i = 0; i < n-2; i++)
            {
                c = a;
                a = b;
                b = c + a;
            }
            return b;
        }
    }

    /*
     *   implement a function that return reverse of a given word
     *   lecture 4 page 19
     */
    public String reverse(String word) {
        return new StringBuilder(word).reverse().toString();
    }

    /*
     *   implement a function that returns true if the given line is
     *   palindrome and false if it is not palindrome.
     *   palindrome is like 'wow', 'never odd or even', 'Wow'
     *   lecture 4 page 19
     */
    public boolean isPalindrome(String line) {
        boolean answer = false;
        String s = "";
        char n ;
        s= line.replace(" " , ""  );
        s = s.toLowerCase(Locale.ROOT);
        String s2 = reverse(s);
        return s.equals(s2);
    }

    /*
     *   implement a function which computes the dot plot of 2 given
     *   string. dot plot of hello and ali is:
     *       h e l l o
     *   h   *
     *   e     *
     *   l       * *
     *   l       * *
     *   o           *
     *   lecture 4 page 26
     */
    public char[][] dotPlot(String str1, String str2) {
        char[][] answer = new char[str1.length()][str2.length()];
        for (int i = 0 ; i < str2.length(); i++)
        {
            for (int j = 0 ; j < str1.length(); j++)
            {
                if (str1.charAt(j) == str2.charAt(i))
                {
                    answer[j][i] = '*';
                }
                else
                {
                    answer[j][i] =  ' ';
                }
            }
        }
        return answer;
    }
}
