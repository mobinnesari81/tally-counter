package sbu.cs;

import java.util.Random;
import java.util.ArrayList;

public class ExerciseLecture5 {

    /*
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14
     */
    public String weakPassword(int length) {
        String answer = "";
        Random random = new Random();
        String alphaBet = "abcdefghijklmnopqrstuvwxyz";
        for (int i = 0 ; i < length ; i++)
        {
            answer += alphaBet.charAt(random.nextInt(26));
        }
        return answer;
    }

    /*
     *   implement a function to create a random password with
     *   given length and at least 1 digit and 1 special character
     *   lecture 5 page 14
     */
    public String strongPassword(int length) throws Exception {
        if (length < 3)
        {
            throw new Exception();
        }
        String password = "";
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        Random random = new Random();
        int specialNumber , digitNumbers;
        specialNumber = (int)(Math.random()*(length-2)+1);
        digitNumbers = (int)(Math.random()*(length-specialNumber-2)+1);
        String digits = "1234567890";
        String specialCharacters = "!@;:?/#$%^&*()_+";
        for (int i = 0 ; i < specialNumber; i++)
        {
            password += specialCharacters.charAt(random.nextInt(specialCharacters.length()));
        }
        for (int i = 0 ; i < digitNumbers; i++)
        {
            password += digits.charAt(random.nextInt(digits.length()));
        }
        for (int i = 0 ; i < length - specialNumber - digitNumbers ; i++)
        {
            password += alphabet.charAt(random.nextInt(alphabet.length()));
        }

        ArrayList<Character> characters = new ArrayList<Character>();
        for (char c:password.toCharArray())
        {
            characters.add(c);
        }
        String output="";
        while(characters.size() != 0)
        {
            int randIndex = (int)(Math.random()*characters.size());
            output += characters.remove(randIndex);
        }
        return output;
    }

    /*
        To compare these two functions, I will start from time order.
        The algorithm which I used at first will run in order n but other algorithm
        because it's random, maybe it couldn't make right password for too long according
        to random conditions. But in memory usage the second algorithm is optimise and better
        than mine.
     */


    public String StrongPasswordTwo(int length) throws Exception
    {
        String chars = "abcdefghijklmnopqrstuvwxyz1234567890-=+/@#!$%^&*";
        String password = "";
        if (length < 3)
        {
            throw new Exception();
        }
        Random random = new Random();
        boolean f = false;
        do {
            for (int i = 0 ; i < length ; i++)
            {
                password += chars.charAt(random.nextInt(chars.length()));
            }
            f = checkStrongPass(password);
        }while (!f);
        return password;
    }

    boolean checkStrongPass(String password) {
        boolean alpha = false, digit = false, special = false;
        String allLower = "abcdefghijklmnopqrstuvwxyz";
        for (char c : password.toCharArray()) {
            if (Character.isDigit(c)) {
                digit = true;
            } else if (allLower.indexOf(Character.toLowerCase(c)) != -1) {
                alpha = true;
            } else {
                special = true;
            }
        }
        return alpha && digit && special;
    }

    /*
     *   implement a function that checks if a integer is a fibobin number
     *   integer n is fibobin is there exist an i where:
     *       n = fib(i) + bin(fib(i))
     *   where fib(i) is the ith fibonacci number and bin(i) is the number
     *   of ones in binary format
     *   lecture 5 page 17
     */
    public boolean isFiboBin(int n){
        int k = 1;
        while (k<=n)
        {
            if (n == fib(k)+counter(decToBin(fib(k))))
            {
                return true;
            }
            k++;
        }
        return false;
    }

    public int fib(int n)
    {
        if (n == 1 || n == 2)
        {
            return 1;
        }
        else
        {
            int a = 1 , b = 1;
            for (int i = 0 ; i < n-2; i++)
            {
                b = summer(a,b);
                a = b - a;
            }
            return b;
        }
    }

    public int counter(String s)
    {
        int answer = 0;
        for (int i = 0 ; i < s.length(); i++)
        {
            if (s.charAt(i) == '1')
            {
                answer++;
            }
        }
        return answer;
    }

    public String decToBin(int n)
    {
        String answer = Integer.toBinaryString(n);
        return answer;
    }

    public int summer(int a , int b)
    {
        return a+b;
    }
}
