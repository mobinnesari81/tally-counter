package sbu.cs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ExerciseLecture6 {

    /*
     *   implement a function that takes an array of int and return sum of
     *   elements at even positions
     *   lecture 6 page  16
     */
    public long calculateEvenSum(int[] arr) {
        long sum = 0;
        for (int i = 0 ; i < arr.length ; i+=2)
        {
            sum += arr[i];
        }
        return sum;
    }

    /*
     *   implement a function that takes an array of int and return that
     *   array in reverse order
     *   lecture 6 page 16
     */
    public int[] reverseArray(int[] arr) {
        int[] answer = new int[arr.length];
        for (int i = 0 ; i < arr.length ; i++ )
        {
            answer[i] = arr[arr.length-1-i];
        }
        return answer;
    }

    /*
     *   implement a function that calculate product of two 2-dim matrices
     *   lecture 6 page 21
     */
    public double[][] matrixProduct(double[][] m1, double[][] m2) throws RuntimeException {
        if (m1[0].length != m2.length)
        {
            throw new RuntimeException();
        }
        double[][] answer = new double[m1.length][m2[0].length];
        double[] row = new double[m1[0].length];
        double[] column = new double[m2.length];
        for(int i = 0 ; i < m1.length ; i++)
        {
            row = m1[i];
            for(int j = 0 ; j < m2[0].length ; j++)
            {
                for (int k = 0 ; k < m2.length ; k++)
                {
                    column[k] = m2[k][j];
                }
                answer[i][j] = arraysSum(row, column);
            }
        }
        return answer;
    }
    public static double arraysSum(double[] array1 , double[] array2)
    {
        double sum = 0;
        for (int i  =0 ; i < array1.length ; i++)
        {
            sum += array1[i] * array2[i];
        }
        return sum;
    }
    /*
     *   implement a function that return array list of array list of string
     *   from a 2-dim string array
     *   lecture 6 page 30
     */
    public List<List<String>> arrayToList(String[][] names) {

        List<List<String>> answer = new ArrayList<List<String>>();
        List<String> row = new ArrayList<String>();
        for (int i = 0 ; i < names.length ; i++)
        {
            for (int j = 0 ; j < names[i].length ; j++)
            {
                row.add(names[i][j]);
            }
            answer.add(new ArrayList<>(row));
            row.clear();
        }
        return answer;
    }

    /*
     *   implement a function that return a list of prime factor of integer n
     *   in ascending order
     *   lecture 6 page 30
     */
    public List<Integer> primeFactors(int n) {
        List<Integer> answer = new ArrayList<>();
        for (int i = 2; i <= n ; i++)
        {
            if (n%i == 0)
            {
                if (isPrime(i))
                {
                    answer.add(i);
                }
            }
        }
        return answer;
    }

    public boolean isPrime(int n)
    {
        for (int i = 2 ; i < n ; i++)
        {
            if (n % i == 0)
            {
                return false;
            }
        }
        return true;
    }

    /*
     *   implement a function that return a list of words in a given string
     *   lecture 6 page 30
     */
    public List<String> extractWord(String line) {
        String[] words = line.split(" ");
        List<String> answer = new ArrayList<String>();
        String s1,s2 = "";
        for (int i = 0 ; i < words.length; i++)
        {
            s1 = words[i];
            for (int j = 0 ;j < s1.length(); j++)
            {
                char c = s1.charAt(j);
                if (Character.isLetter(c))
                {
                    s2 += c;
                }
            }
            answer.add(s2);
            s2 = "";
        }
        return answer;
    }


}
